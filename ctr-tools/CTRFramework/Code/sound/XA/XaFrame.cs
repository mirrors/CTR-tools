﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTRFramework.Sound
{
    public class XaFrame
    {
        public byte shift = 0;
        public byte filter = 0;
        public byte[] data = new byte[28];
    }
}